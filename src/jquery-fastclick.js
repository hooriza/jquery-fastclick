// jQuery.fastClick
jQuery.fastClick = (function($) {

	var config = {
		tapHoldDuration : 1000,
		consecutiveInterval : 500,
		moveThreshold : 10,
		activableSelector : 'button a',
		activedClassName : 'actived'
	};

	var info = null;
	var prevents = [];

	var consecutiveTarget, consecutiveTime = -Infinity, consecutiveCounts;

	var findPrevent = function(target) {
		for (var i = 0, prevent; !!(prevent = prevents[i]); i++) {
			if (prevent.target === target) {
				prevents.splice(i, 1);
				return prevent;
			}
		}
		return null;
	};

	var clearInfo = function() {
		if (!info) { return; }

		if (info.timer) { clearTimeout(info.timer); }

		info.$button.removeClass(config.activedClassName);

		info.$this.
			off('touchmove', onTouchMove).
			off('touchend', onTouchEnd).
			off('touchcancel', onTouchCancel).
			off('click', onClick);

		info = null;
	};

	$.event.special.fastclick = $.event.special.taphold = $.event.special.fastclick = {
		setup: function(data) {

			var $this = $(this);
			var attachedCount = $this.data('fastclick-attached') || 0;

			if (attachedCount === 0) {
				$this.on('touchstart', onTouchStart);
			}

			$this.data('fastclick-attached', attachedCount + 1);

		},
		teardown: function() {

			var $this = $(this);
			var attachedCount = $this.data('fastclick-attached') || 0;

			if (attachedCount === 1) {
				$this.off('touchstart', onTouchStart);
			}

			$this.data('fastclick-attached', attachedCount - 1);

		}
	};

	function onTouchStart(evt) {

		var touch = evt.originalEvent.touches[0];

		var $target = $(touch.target);
		var $button = $target.closest(config.activableSelector);

		info = {
			pos : [ touch.pageX, touch.pageY ],
			$this : $(this),
			$target : $target,
			$button : $button
		};

		info.timer = setTimeout(function() {
			info.$target.trigger('taphold');
			clearInfo();
		}, config.tapHoldDuration);

		$button.addClass(config.activedClassName);

		info.$this.
			on('touchmove', onTouchMove).
			on('touchend', onTouchEnd).
			on('touchcancel', onTouchCancel).
			on('click', onClick);

	}

	function onTouchMove(evt) {

		if (!info) { return; }

		var touch = (evt.originalEvent || evt).touches[0];
		var distance = Math.pow(info.pos[0] - touch.pageX, 2) + Math.pow(info.pos[1] - touch.pageY, 2);

		if (distance >= config.moveThreshold) {
			clearInfo();
		}

	}

	function onTouchEnd(evt) {

		if (!info) { return; }

		var target = info.$target[0];
		var customEvent = $.Event('fastclick');

		if (Date.now() - consecutiveTime > config.consecutiveInterval || target !== consecutiveTarget) { consecutiveCounts = 0; }

		customEvent.detail = ++consecutiveCounts;
		info.$target.trigger(customEvent);

		findPrevent(target);

		consecutiveTime = Date.now();
		consecutiveTarget = target;

		var prevented = customEvent.isDefaultPrevented();

		prevents.push({
			target : target,
			prevented : prevented
		});

		clearInfo();

		if (prevented) {
			evt.preventDefault();
		}

	}

	function onTouchCancel(evt) {
		if (!info) { return; }
		clearInfo();
	}

	function onClick(evt) {
		var prevent = findPrevent(evt.target);
		if (prevent && prevent.prevented) {
			evt.preventDefault();
		}
	}

	// 스크롤을 멈추려고 눌렀을때 클릭되는 문제
	document.addEventListener('scroll', clearInfo, true);	

	return config;

})(jQuery);